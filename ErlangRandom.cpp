/* 
 * File:   ErlangRandom.cpp
 * Author: dani
 * 
 * Created on 7 de abril de 2013, 17:48
 */

#include "ErlangRandom.h"

ErlangRandom::ErlangRandom(double alpha, double beta): Random(beta) {
    this->alpha = alpha;
}

double ErlangRandom::getNextRand(){
    double u = rand()/RAND_MAX;
    for(int i = 1; i < alpha; i++){        
        u *= ((double)rand())/RAND_MAX;
    }
    double x = -log(u)/beta;
    
    return x;
}

ErlangRandom::~ErlangRandom() {
}

