
#include "Client.h"


Client::Client(unsigned int systemEntryTime){
    this->systemEntryTime = systemEntryTime;
}

Client::Client(const Client & orig){
    this->systemEntryTime = orig.systemEntryTime;
    this->serverEntryTime = orig.serverEntryTime;
}
   
unsigned int Client::getSystemEntryTime(){
    return systemEntryTime;
}
    
void Client::setServerEntryTime(unsigned int serverEntryTime){
    this->serverEntryTime = serverEntryTime;
}

unsigned int Client::getServerEntryTime(){
    return systemEntryTime;
}    
 
Client::~Client(){
    
}
