#ifndef CLIENTDISPENSER_H
#define	CLIENTDISPENSER_H

#include "Clock.h"
#include "InternalServer.h"
#include "Random.h"

/**
 * Dispenses clients into the system.
 */
class ClientDispenser: public ClockObserver
{
public:
    ClientDispenser(Clock &clock, InternalServer &server, Random& random);
    
    /** Creates a new client and sends it to a server*/
    void notifyTick(double time);
    
    virtual ~ClientDispenser();
    
private:
    /** The time on which a new client will be dispatched*/
    double dispenseTime;
    
    /** Random generator*/
    Random* random;
    
    /** Reference to the event clock*/
    Clock* clock;
    
    /** Server to be dispensed*/
    InternalServer* server;
};


#endif	/* CLIENTDISPENSER_H */

