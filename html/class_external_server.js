var class_external_server =
[
    [ "ExternalServer", "class_external_server.html#a159450b440bd4db7beb3774829f016b7", null ],
    [ "ExternalServer", "class_external_server.html#a954ddc04f241564cf31d7e853b26ad8a", null ],
    [ "~ExternalServer", "class_external_server.html#a14de1389e32387d9759c569a21c1a3bf", null ],
    [ "addObserver", "class_external_server.html#a279d2d4c2048d3d471165018685d4592", null ],
    [ "getAverageClientSystemStay", "class_external_server.html#a9269875818495ee2698f4115b11baff8", null ],
    [ "isRoundFinished", "class_external_server.html#abc67d437f33cdcacdb184398719d1cce", null ],
    [ "receiveClient", "class_external_server.html#a8112d0803599c1363870264a52f80d67", null ]
];