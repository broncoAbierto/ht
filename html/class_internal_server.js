var class_internal_server =
[
    [ "InternalServer", "class_internal_server.html#ad1c768a0b48e43cd08f389c7ab205db1", null ],
    [ "InternalServer", "class_internal_server.html#a47c929d90d9789a0a9899869019b8f65", null ],
    [ "InternalServer", "class_internal_server.html#ad73276ddf7fdbc3d44f0e44b41fc3a09", null ],
    [ "~InternalServer", "class_internal_server.html#a6c1b2fa7c67751ae86995f85fb7bf247", null ],
    [ "getID", "class_internal_server.html#a0aa56aad3b60883dc5810abf7be777a3", null ],
    [ "getServeTime", "class_internal_server.html#a8d3c136d5e22310737504eb0b69a1986", null ],
    [ "notifyTick", "class_internal_server.html#ae59d9ed6d4f94ed7df36a596a4b479c8", null ],
    [ "receiveClient", "class_internal_server.html#a5fdb1feeb8209f3d43d3971df80656e0", null ],
    [ "clientQueue", "class_internal_server.html#ae2a4df76e330431fdaf996293196cf40", null ],
    [ "QUEUE", "class_internal_server.html#a79c2f89ebf146264e26b580368b8ec8d", null ],
    [ "serveTime", "class_internal_server.html#a8bafbf961cef1482d87e4cab53db3997", null ]
];