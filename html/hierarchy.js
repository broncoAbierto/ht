var hierarchy =
[
    [ "Client", "class_client.html", null ],
    [ "Clock", "class_clock.html", null ],
    [ "ClockObserver", "class_clock_observer.html", [
      [ "ClientDispenser", "class_client_dispenser.html", null ],
      [ "InternalServer", "class_internal_server.html", [
        [ "LoggableInternalServer", "class_loggable_internal_server.html", null ]
      ] ]
    ] ],
    [ "Log", "class_log.html", null ],
    [ "Logger", "class_logger.html", null ],
    [ "Random", "class_random.html", [
      [ "ErlangRandom", "class_erlang_random.html", null ],
      [ "ExponentialRandom", "class_exponential_random.html", null ]
    ] ],
    [ "RoundObserver", "class_round_observer.html", [
      [ "LoggableInternalServer", "class_loggable_internal_server.html", null ]
    ] ],
    [ "Server", "class_server.html", [
      [ "ExternalServer", "class_external_server.html", null ],
      [ "InternalServer", "class_internal_server.html", null ]
    ] ],
    [ "Simulatron", "class_simulatron.html", null ]
];