var class_clock =
[
    [ "Clock", "class_clock.html#adbc370eb6b5f8d01645cf440188160a8", null ],
    [ "~Clock", "class_clock.html#afc976ce68fa85e15cc06f9ed47bddb7c", null ],
    [ "addEventTime", "class_clock.html#aad01f875ab0b057cc5e3e59c5021c4b9", null ],
    [ "addObserver", "class_clock.html#acd0304f8c6fbd73693635456059c3958", null ],
    [ "getTime", "class_clock.html#aa1f7236008c3abc5f5766d62d81e85fc", null ],
    [ "tick", "class_clock.html#a116757f0e1575348549b1f2fc346d492", null ]
];