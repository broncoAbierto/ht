#ifndef CLOCKOBSERVER_H
#define	CLOCKOBSERVER_H

/**
 * Observes clock events.
 */
class ClockObserver
{
public:
    /** Notifies a new tick of the clock*/
    virtual void notifyTick(double time) = 0;
    
    virtual ~ClockObserver() {}
    
};


#endif	/* CLOCKOBSERVER_H */

