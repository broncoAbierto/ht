#include "LoggableInternalServer.h"

LoggableInternalServer::LoggableInternalServer(unsigned int id, Clock& clock, Random &random)
: InternalServer(id, clock, random) {
    this->init();
}

LoggableInternalServer::LoggableInternalServer(unsigned int id, Clock& clock, Random &random, int queue)
: InternalServer(id, clock, random, queue) {
    this->init();
}

LoggableInternalServer::LoggableInternalServer(unsigned int id, Clock& clock, Random &random, int queue, vector<Server *> &adjacentServers)
: InternalServer(id, clock, random, queue, adjacentServers) {
    this->init();
}

void LoggableInternalServer::init() {
    clientsDispatched = 0;
    lostClients = 0;
    accumulatedClientStay = 0.;
    clientNumberDuration = new map<int, double>;
    lastClientNumModificationTime = 0;
    startTime = lastClientNumModificationTime;
}

void LoggableInternalServer::notifyTick(double time) {
    if (serveTime == time) {
        if(!clientQueue->empty()){
            //Retrieve the client to be served
            Client *servedClient = clientQueue->front();

            //Update the accumulated client stay with the time the client spent in the server
            accumulatedClientStay += clock->getTime() - servedClient->getServerEntryTime();

            clientsDispatched++;

            //Update the time that the system has spent with a certain number of clients
            int clients = clientQueue->size();
            (*clientNumberDuration)[clients] += clock->getTime() -
                    lastClientNumModificationTime;

            lastClientNumModificationTime = clock->getTime();
        }
        InternalServer::notifyTick(time);
        
    }
}

void LoggableInternalServer::receiveClient(Client& client) {
    if (QUEUE != -1 && clientQueue->size() > QUEUE) {
        lostClients++;
    } else {
        //Update the time that the system has spent with a certain number of clients
        (*clientNumberDuration)[clientQueue->size()] += clock->getTime() - 
                lastClientNumModificationTime;
        lastClientNumModificationTime = clock->getTime();
    }

    InternalServer::receiveClient(client);
}

void LoggableInternalServer::notifyTransitoryPeriodEnd() {
    clientsDispatched = 0;
    lostClients = 0;
    accumulatedClientStay = 0.;
    delete clientNumberDuration;
    clientNumberDuration = new map<int, double>;
    lastClientNumModificationTime = clock->getTime();
    startTime = lastClientNumModificationTime;
}

double LoggableInternalServer::calculClientNumberProbability(int numberOfClients){
    double time = 0.;
    //Flush the current state
    (*clientNumberDuration)[clientQueue->size()] += 
            clock->getTime() - lastClientNumModificationTime;
    
    for(int i = 0; i <= numberOfClients; ++i){
        time += (*clientNumberDuration)[i];
    }
    
    return time/(clock->getTime() - startTime);
}

double LoggableInternalServer::calculClientLostProbability(){
    return (double)lostClients/clientsDispatched;
}

double LoggableInternalServer::averageClientStay(int totalNumberOfClients){    
    return this->totalClientStay() / totalNumberOfClients;
}

double LoggableInternalServer::totalClientStay(){
    double time = 0;
    for(int i = 1; i < 10; i++){
            time += (*clientNumberDuration)[i] * i;
        }    
    return time;
}

double LoggableInternalServer::averageClientCount(){
    double average = 0;    
    for(int i = 1; i < 10; i++){
        double iProb = this->calculClientNumberProbability(i) 
                - this->calculClientNumberProbability(i-1);
        average += i * iProb;    
    }
    return average;
}

LoggableInternalServer::~LoggableInternalServer() {
    delete clientNumberDuration;
}