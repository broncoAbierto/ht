#ifndef SERVER_H
#define	SERVER_H

#include <vector>
#include "Clock.h"
#include "Client.h"
using namespace std;

/**
 * A server which is capable of recieveing clients.
 */
class Server
{
public:
    
    Server(Clock &clock);
    
    Server(Clock &clock, vector<Server *> &adjacentServers);
    
    /** Recieves a client from another server. */
    virtual void receiveClient(Client& client) = 0;  
    
    /** Adds an adjacent server*/
    void addAdjacent(Server& server);
    
    ~Server();
    
protected:
    /** Servers to which is able to send a client*/
    vector<Server *> *adjacentServers;
    
    /** Reference to the event clock*/
    Clock* clock;
};

#endif	/* SERVER_H */

