/* 
 * File:   Logger.cpp
 * Author: dani
 * 
 * Created on 7 de abril de 2013, 22:13
 */

#include "Logger.h"

Logger::Logger(string title) {
    this->title = title;
    firstTime = true;
}

void Logger::log(string message){
    if(firstTime){
        firstTime = false;
        cout << title << endl;
    }
    
    cout << '\t' << message << endl;
}

Logger::~Logger() {
}

