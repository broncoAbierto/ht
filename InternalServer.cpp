#include "InternalServer.h"
#include <iostream>

InternalServer::InternalServer(unsigned int id, Clock& clock, Random &random)
: Server(clock), QUEUE(-1) {
    this->init(id, random);
}

InternalServer::InternalServer(unsigned int id, Clock& clock, Random &random, int queue)
: Server(clock), QUEUE(queue){
    this->init(id, random);
}

InternalServer::InternalServer(unsigned int id, Clock& clock, Random &random, int queue, vector<Server *> &adjacentServers)
: Server(clock, adjacentServers), QUEUE(queue) {
    this->init(id, random);
}

void InternalServer::init(unsigned int id, Random& random) {
    this->id = id;
    this->random = &random;
    clientQueue = new queue<Client *>();
}

void InternalServer::receiveClient(Client& client) {
//    std::cout << "I'm Server " << id << " and I'm receiving a client " << endl;
//    std::cout << "Space available: " << (QUEUE == -1 || clientQueue->size() <= QUEUE) << endl;
    
    //If the client queue was empty at the arrival of this client,
    //no service events were being calculated, so the next one must be retrieved
    if(clientQueue->empty()){
        double currentTime = clock->getTime();
        serveTime = currentTime + random->getNextRand();
        clock->addEventTime(serveTime);
    }
    
    if (QUEUE == -1 || clientQueue->size() <= QUEUE) {
        client.setServerEntryTime(clock->getTime());
        clientQueue->push(&client);
    } else {
        delete &client;
    }
}

void InternalServer::notifyTick(double time) {
//    std::cout << "I'm an Internal Server and I'll serve at " << serveTime << " but it's still " << time << endl;
//    std::cout << "Client queue empty? " << clientQueue->empty() << endl;
    if(!clientQueue->empty() && serveTime == time) {
        int i = 0;
        unsigned int rand = random->getUniformRand(adjacentServers->size());
        
        //Add the new event
        serveTime = time + random->getNextRand();
        clock->addEventTime(serveTime);
       
        
        
//      std::cout << "I'm Server " << id << " and I'm serving my connection number" ;
        //Move the client to another server
        Client *servedClient = new Client(*(clientQueue->front()));
        clientQueue->pop();

        vector<Server *>::iterator it = adjacentServers->begin();
        while (it != adjacentServers->end()) {
            if(i == rand){
//                    std::cout << i << endl;
                (*it)->receiveClient(*servedClient);
            }
            ++it;
            ++i;
        }
        
        
    }
}

unsigned int InternalServer::getID() {
    return id;
}

double InternalServer::getServeTime() {
    return serveTime;
}

InternalServer::~InternalServer() {
    delete clientQueue;
}