#include "ExternalServer.h"
#include "RoundObserver.h"
#include <iostream>

ExternalServer::ExternalServer(Clock& clock) : Server(clock) {
    this->init();
}

ExternalServer::ExternalServer(Clock& clock, vector<RoundObserver *> &observers) 
: Server(clock) {
    this->init();
}

void ExternalServer::init(){
    accumulatedClientStay = 0;
    servedClients = 0;
    transitoryPeriod = true;
    observers = new vector<RoundObserver *>;
}

void ExternalServer::addObserver(RoundObserver& observer){
    observers->push_back(&observer);
}

void ExternalServer::receiveClient(Client& client) {
    accumulatedClientStay += clock->getTime() - client.getSystemEntryTime();
    servedClients++;
   
    delete &client;
}

double ExternalServer::getAverageClientSystemStay(){
    return (double)accumulatedClientStay/servedClients;
}

bool ExternalServer::isRoundFinished() {
    if (!transitoryPeriod) {
        if (servedClients >= MAX_PER_ROUND) {
            transitoryPeriod = !transitoryPeriod;
            return true;
        }
        return false;
    } else {
        if (servedClients >= MAX_PER_TRANSITORY_PERIOD) {
            transitoryPeriod = !transitoryPeriod;
            //reset variables
            accumulatedClientStay = 0;
            servedClients = 0;

            //Notify it to all observers
            vector<RoundObserver *>::iterator itr = observers->begin();
            while (itr != observers->end()) {
                (*itr)->notifyTransitoryPeriodEnd();
                ++itr;
            }   
        }
        return false;
    }
}

ExternalServer::~ExternalServer() {
    delete observers;
}