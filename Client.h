#ifndef CLIENT_H
#define	CLIENT_H

/**
 * Client of the system. 
 * Used for measuring time spent in the system by clients.
 */
class Client
{
public:
    Client(unsigned int systemEntryTime);
    
    Client(const Client & orig);
    
    /** returns the time of entry into the system*/
    unsigned int getSystemEntryTime();
    
    /** returns the time of entry into a subsystem*/
    void setServerEntryTime(unsigned int serverEntryTime);
    
    /** returns the time of entry into a subsystem*/
    unsigned int getServerEntryTime();
    
    virtual ~Client();
    
private:
    /** Time when the client entered the system*/
    unsigned int systemEntryTime;
    
    /** Time when the client entered a subsystem*/
    unsigned int serverEntryTime;

};

#endif	/* CLIENT_H */

