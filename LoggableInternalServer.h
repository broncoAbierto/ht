#ifndef LOGGABLEINTERNALSERVER_H
#define	LOGGABLEINTERNALSERVER_H

#include <map>
#include <iostream>
#include "InternalServer.h"
#include "RoundObserver.h"
using namespace std;

/** 
 * An internal server of the system which logs some of its properties.
 */
class LoggableInternalServer : public InternalServer, public RoundObserver {
public:

    LoggableInternalServer(unsigned int id, Clock& clock, Random &random);

    /** @param queue = -1 implies infinite queue.*/
    LoggableInternalServer(unsigned int id, Clock& clock, Random &random, int queue);

    /** @param queue = -1 implies infinite queue.*/
    LoggableInternalServer(unsigned int id, Clock& clock, Random &random, int queue, vector<Server *> &adjacentServers);

    /** Receives a client from another server. */
    void receiveClient(Client& client);

    /** Sends a client to another server*/
    void notifyTick(double time);

    /** Clears the state of the logging properties*/
    void notifyTransitoryPeriodEnd();

    /** Calculates the probability of having <= numberOfClients*/
    double calculClientNumberProbability(int numberOfClients);

    /** Calculates the probability of loosing a client*/
    double calculClientLostProbability();

    /** Calculates the average stay time of a client in a subsystem*/
    double averageClientStay(int totalNumberOfClients);
    
    /** Calculates the total stay time of a client in a subsystem*/
    double totalClientStay();
    
    /** Calculates the total stay time of a client in a subsystem*/
    double averageClientCount();    
    
    ~LoggableInternalServer();

private:
    /** Number of clients dispatched*/
    unsigned int clientsDispatched;

    /** Number of lost clients*/
    unsigned int lostClients;

    /** Total time spent in this server by its clients*/
    double accumulatedClientStay;

    /** Last time when the number of clients changed*/
    double lastClientNumModificationTime;

    /** Map of the number of clients and the time the system stayed that way */
    map<int, double> *clientNumberDuration;
    
    /** Time when the round started*/
    double startTime;
    
    void init();

};


#endif	/* LOGGABLEINTERNALSERVER_H */

