#include "Simulatron.h"
#include "LoggableInternalServer.h"
#include "ExponentialRandom.h"
#include "ClientDispenser.h"
#include "Logger.h"
#include <iostream>
#include <string>
#include <math.h>

using namespace std;

Simulatron::Simulatron() {
}

void Simulatron::start() {

    /** Simulation elements */
    Clock* clock = new Clock();

    InternalServer* server1 = new InternalServer(1, *clock, *(new ExponentialRandom(1)));

    InternalServer* server2 = new InternalServer(2, *clock, *(new ExponentialRandom(1)));
    InternalServer* server3 = new InternalServer(3, *clock, *(new ExponentialRandom(1)));
    InternalServer* server5 = new InternalServer(5, *clock, *(new ExponentialRandom(2)));

    LoggableInternalServer* server4 = new LoggableInternalServer(4, *clock, *(new ExponentialRandom(2)));
    LoggableInternalServer* server6 = new LoggableInternalServer(6, *clock, *(new ExponentialRandom(5)), 8);

    ExternalServer* outerSpace = new ExternalServer(*clock);

    ClientDispenser* dispenser1 = new ClientDispenser(*clock, *server1, *(new ExponentialRandom(0.9)));
    ClientDispenser* dispenser2 = new ClientDispenser(*clock, *server2, *(new ExponentialRandom(0.8)));
    ClientDispenser* dispenser3 = new ClientDispenser(*clock, *server3, *(new ExponentialRandom(0.7)));

    /** Connections between servers */
    server1->addAdjacent(*server4);
    server1->addAdjacent(*server5);
    server2->addAdjacent(*server4);
    server2->addAdjacent(*server5);
    server3->addAdjacent(*server4);
    server3->addAdjacent(*server5);
    server4->addAdjacent(*server6);
    server5->addAdjacent(*server6);
    server6->addAdjacent(*outerSpace);

    /** External server observers*/
    outerSpace->addObserver(*server4);
    outerSpace->addObserver(*server6);

    /** Clock observers*/
    clock->addObserver(*server1);
    clock->addObserver(*server2);
    clock->addObserver(*server3);
    clock->addObserver(*server4);
    clock->addObserver(*server5);
    clock->addObserver(*server6);
    clock->addObserver(*dispenser1);
    clock->addObserver(*dispenser2);
    clock->addObserver(*dispenser3);

    /** Simulation loop */
    double sumS4lessThan2 = 0;
    double sumS6clientLost = 0;
    double specificTotalTimeOn6[SIMULATIONS];    
    double specificW6[SIMULATIONS];    
    double specificL6[SIMULATIONS];    
    //Igual mejor un get de la longitud de la cola...
    double clientsOnS6[10];
    
    //Addition of L in server 6 on each iteration
    double accumulatedL6 = 0;
    
    //Addition of W in server 6 on each iteration
    double accumulatedW6 = 0;
    
    //Initialize arrays
    for(int i = 0; i < 10; i++){
        clientsOnS6[i] = 0;        
    }
    
    for (int i = 0; i < SIMULATIONS; i++) {        
        while (!outerSpace->isRoundFinished()) {
            clock->tick();
        }
        
        //Log        
        sumS4lessThan2 += server4->calculClientNumberProbability(1);
        sumS6clientLost += server6->calculClientLostProbability();
        specificTotalTimeOn6[i] = server6->totalClientStay();
        specificW6[i] = server6->averageClientStay(CLIENTS);
        specificL6[i] = server6->averageClientCount();
        accumulatedW6 += specificW6[i];
        accumulatedL6 += specificL6[i];
                
        for(int i = 0; i < 10; i++){
                clientsOnS6[i] += server6->calculClientNumberProbability(i);
        }

    }

    /**
     * Server 4
     */
    cout << "Server " << server4->getID() << endl;
    cout << '\t' << "Probability of having less than 2 clients: " <<
            sumS4lessThan2/SIMULATIONS << endl;

    cout << endl;

    /**
     * Server 6
     */
    cout << "Server " << server6->getID() << endl;
    cout << '\t' << "Probability of losing a client: " <<
             sumS6clientLost/SIMULATIONS << endl << endl;

    cout << '\t' << "Probability of having " << 0 << " clients: " <<
                 clientsOnS6[0]/SIMULATIONS << endl << endl;
    
    //Probability of having n clients  
    for(int i = 1; i < 10; i++){
        double iProb = clientsOnS6[i]/SIMULATIONS - clientsOnS6[i-1]/SIMULATIONS;
        cout << '\t' << "Probability of having " << i << " clients: " << iProb
                  << endl << endl;
    }
   
    //Average client count in the subsystem L6
    cout << '\t' << "Average client count in the system (L6): " << 
            accumulatedL6 / SIMULATIONS << endl << endl;    
    
    //Average client time in the subsystem W6
    cout << '\t' << "Average client time in the subsystem (W6): " << 
                accumulatedW6 / SIMULATIONS << endl << endl;
        
    //Confidence interval at 90% for L6
    double estimateOfL6 = accumulatedL6 / SIMULATIONS;
    double variance = 0;
    for(int i = 0; i < SIMULATIONS; i++){
        variance += (pow(specificL6[i] - estimateOfL6, 2))/(SIMULATIONS - 1);
    }
    double stdDeviation = sqrt(variance);
    double amplitude = 1.71 * stdDeviation / sqrt(SIMULATIONS);
    double relError = amplitude / estimateOfL6;
    double lowerLimit = estimateOfL6 - amplitude;
    double upperLimit = estimateOfL6 + amplitude;
    
    cout << '\t' << "Confidence interval at 90% for L6: (" << 
             lowerLimit << ", " << upperLimit << ")" << endl;
    cout << '\t' << "Relative error: " << relError << endl << endl;
    
    //Confidence interval at 90% for W6
    double estimateOfW6 = accumulatedW6 / SIMULATIONS;
    variance = 0;
    for(int i = 0; i < SIMULATIONS; i++){
        variance += (pow(specificW6[i] - estimateOfW6, 2))/(SIMULATIONS - 1);
    }
    stdDeviation = sqrt(variance);
    amplitude = 1.71 * stdDeviation / sqrt(SIMULATIONS);
    relError = amplitude / estimateOfW6;
    lowerLimit = estimateOfW6 - amplitude;
    upperLimit = estimateOfW6 + amplitude;
    
    cout << '\t' << "Confidence interval at 90% for W6: (" << 
         lowerLimit << ", " << upperLimit << ")" << endl;
    cout << '\t' << "Relative error: " << relError << endl << endl;

    
    /**
     * Whole system
     */
    //Average client time in the whole system (W)
    cout << "Average client time in the whole system (W): " << 
            outerSpace->getAverageClientSystemStay() << endl << endl;
    

    /** End simulation */
    delete clock;
    delete server1;
    delete server2;
    delete server3;
    delete server4;
    delete server5;
    delete server6;
    delete outerSpace;

}

Simulatron::~Simulatron() {

}

