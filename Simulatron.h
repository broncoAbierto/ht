#ifndef SIMULATRON_H
#define	SIMULATRON_H

#include <vector>
#include "InternalServer.h"
#include "ExternalServer.h"
using namespace std;

/**
 * Application class.
 */
class Simulatron {
public:
    Simulatron();

    /** Starts the simulation*/
    void start();

    virtual ~Simulatron();
private:
    static const unsigned int SIMULATIONS = 25;
    static const unsigned int CLIENTS = 10000;
    
    /** Calculs the average stay time of a client in a system*/
    float averageSystemClientStay(ExternalServer const &server);
     
};

#endif	/* SIMULATRON_H */

