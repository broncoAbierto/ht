#ifndef EXTERNALSERVER_H
#define	EXTERNALSERVER_H

#include "Server.h"
#include "RoundObserver.h"
#include <vector>
using namespace std;

/**
 * Drain.
 * Used for determining when the simulation has ended and for performing some
 * calculus. 
 */
class ExternalServer : public Server {
public:
    ExternalServer(Clock& clock);

    ExternalServer(Clock& clock, vector<RoundObserver *> &observers);

    /** Adds a observer*/
    void addObserver(RoundObserver& observer);

    /** Recieves a client from another server. */
    void receiveClient(Client& client);

    /** Returns true if the round (transitory + simulation) is finished. */
    bool isRoundFinished();
    
    double getAverageClientSystemStay();

    ~ExternalServer();

private:
    /** Maximum number of clients per round. */
    static const unsigned int MAX_PER_ROUND = 10000;

    /** Maximum number of clients per transitory period. */
    static const unsigned int MAX_PER_TRANSITORY_PERIOD = 500;

    /** Accumulation of the time spent in the system by its clients. */
    unsigned long accumulatedClientStay;

    /** true if we are currently in transitory period. */
    bool transitoryPeriod;

    /** Number of clients that have passed*/
    unsigned int servedClients;

    /** Round observers*/
    vector<RoundObserver *> *observers;
    
    void init();

};


#endif	/* EXTERNALSERVER_H */

