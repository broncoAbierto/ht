#ifndef ROUNDOBSERVER_H
#define	ROUNDOBSERVER_H

/** Obsevers of rounds*/
class RoundObserver {
public:
        
    /** Notifies the end of the transitory period*/
    virtual void notifyTransitoryPeriodEnd() = 0;
    
    virtual ~RoundObserver() {};

};

#endif	/* ROUNDOBSERVER_H */

