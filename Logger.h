/* 
 * File:   Logger.h
 * Author: dani
 *
 * Created on 7 de abril de 2013, 22:13
 */

#ifndef LOGGER_H
#define	LOGGER_H

#include <string>
#include <iostream>
using namespace std;

class Logger {
public:
    Logger(string title);
    
    void log(string message);

    virtual ~Logger();
private:
    string title;
    bool firstTime;
};

#endif	/* LOGGER_H */

