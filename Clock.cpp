#include "Clock.h"
#include "ClockObserver.h"

Clock::Clock(){
    time = 0;
    observers = new vector<ClockObserver *>;
    eventQueue = new set<double>;
    eventQueue->insert(time);
}

/** Returns current time.*/
double Clock::getTime(){
    return time;
}

/** Adds the time of a new event to the queue.*/
void Clock::addEventTime(double time){
    eventQueue->insert(time);
}

/** Adds an observer*/
void Clock::addObserver(ClockObserver& observer){
    observers->push_back(&observer);
}

void Clock::tick(){
    //Get the time of the next event
    set<double>::iterator nextEvent = eventQueue->begin();
    time = *nextEvent;
    eventQueue->erase(nextEvent);
    
    //Notify it to allobservers
    vector<ClockObserver *>::iterator itr = observers->begin();    
    while(itr != observers->end()){
        (*itr)->notifyTick(time);
        ++itr;
    }
    
}

Clock::~Clock(){
    delete eventQueue;
    delete observers;
}