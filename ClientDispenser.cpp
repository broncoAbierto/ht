#include "ClientDispenser.h"
#include <iostream>

ClientDispenser::ClientDispenser(Clock& clock, InternalServer& server, Random& random){
    this->clock = &clock;
    this->server = &server;
    this->random = &random;
}

void ClientDispenser::notifyTick(double time){
    //if it is the initial state, there is no event, we must add one
    if(time == 0){
        dispenseTime = time + random->getNextRand();
        clock->addEventTime(dispenseTime);
    } else if(dispenseTime == time){
        //Send a new client to the server
        Client* client = new Client(time);
        server->receiveClient(*client);
        
        dispenseTime = time + random->getNextRand();
        clock->addEventTime(dispenseTime);
    }
}

ClientDispenser::~ClientDispenser(){
    delete random;
}