#include "Server.h"

Server::Server(Clock& clock){
    this->clock = &clock;
    this->adjacentServers = new vector<Server *>;
}

Server::Server(Clock& clock, vector<Server *> &adjacentServers){
    this->clock = &clock;
    this->adjacentServers = &adjacentServers;
}

void Server::addAdjacent(Server& server){
    adjacentServers->push_back(&server);
}

Server::~Server() {
    delete adjacentServers;
}

