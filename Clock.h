#ifndef CLOCK_H
#define	CLOCK_H

#include <set>
#include <vector>
#include "ClockObserver.h"
using namespace std;

/**
 * Event clock.
 * In charge of updating the time of the system.
 */
class Clock
{    
public:
    Clock();
    
    /** Returns current time.*/
    double getTime();
    
    /** Adds a new time of a future event.*/
    void addEventTime(double time);
    
    /** Adds a observer*/
    void addObserver(ClockObserver& observer);
    
    /** Increases the current time */
    void tick();
    
    ~Clock();
    
private:
    /** Current time.*/
    double time;
    
    /**Auto-ordered event queue*/
    set<double> *eventQueue;
    
    /** Clock observers*/
    vector<ClockObserver *> *observers;
};


#endif	/* CLOCK_H */

