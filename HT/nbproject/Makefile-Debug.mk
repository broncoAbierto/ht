#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1472/Client.o \
	${OBJECTDIR}/_ext/1472/ClientDispenser.o \
	${OBJECTDIR}/_ext/1472/Clock.o \
	${OBJECTDIR}/_ext/1472/ErlangRandom.o \
	${OBJECTDIR}/_ext/1472/ExponentialRandom.o \
	${OBJECTDIR}/_ext/1472/ExternalServer.o \
	${OBJECTDIR}/_ext/1472/InternalServer.o \
	${OBJECTDIR}/_ext/1472/LoggableInternalServer.o \
	${OBJECTDIR}/_ext/1472/Logger.o \
	${OBJECTDIR}/_ext/1472/Random.o \
	${OBJECTDIR}/_ext/1472/RoundObserver.o \
	${OBJECTDIR}/_ext/1472/Server.o \
	${OBJECTDIR}/_ext/1472/Simulatron.o \
	${OBJECTDIR}/_ext/1472/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ht.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ht.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ht ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/1472/Client.o: ../Client.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/Client.o ../Client.cpp

${OBJECTDIR}/_ext/1472/ClientDispenser.o: ../ClientDispenser.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/ClientDispenser.o ../ClientDispenser.cpp

${OBJECTDIR}/_ext/1472/Clock.o: ../Clock.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/Clock.o ../Clock.cpp

${OBJECTDIR}/_ext/1472/ErlangRandom.o: ../ErlangRandom.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/ErlangRandom.o ../ErlangRandom.cpp

${OBJECTDIR}/_ext/1472/ExponentialRandom.o: ../ExponentialRandom.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/ExponentialRandom.o ../ExponentialRandom.cpp

${OBJECTDIR}/_ext/1472/ExternalServer.o: ../ExternalServer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/ExternalServer.o ../ExternalServer.cpp

${OBJECTDIR}/_ext/1472/InternalServer.o: ../InternalServer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/InternalServer.o ../InternalServer.cpp

${OBJECTDIR}/_ext/1472/LoggableInternalServer.o: ../LoggableInternalServer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/LoggableInternalServer.o ../LoggableInternalServer.cpp

${OBJECTDIR}/_ext/1472/Logger.o: ../Logger.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/Logger.o ../Logger.cpp

${OBJECTDIR}/_ext/1472/Random.o: ../Random.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/Random.o ../Random.cpp

${OBJECTDIR}/_ext/1472/RoundObserver.o: ../RoundObserver.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/RoundObserver.o ../RoundObserver.cpp

${OBJECTDIR}/_ext/1472/Server.o: ../Server.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/Server.o ../Server.cpp

${OBJECTDIR}/_ext/1472/Simulatron.o: ../Simulatron.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/Simulatron.o ../Simulatron.cpp

${OBJECTDIR}/_ext/1472/main.o: ../main.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1472/main.o ../main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ht.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
