#ifndef RANDOM_H
#define	RANDOM_H

#include <cstdlib>
#include <stdlib.h>
#include <time.h>
#include <cmath>

/**
 * Generates random numbers.
 */
class Random
{
public:
    Random(double beta);
    
    /** Creates and returns a new random double*/
    virtual double getNextRand() = 0;
    
    /** Creates and returns a new uniformly distributed random integer between
     *  0 and max (exclusive)*/
    unsigned int getUniformRand(int max);
        
    virtual ~Random();
    
protected:
    double beta;

};


#endif	/* RANDOM_H */

