#ifndef EXPONENTIALRANDOM_H
#define	EXPONENTIALRANDOM_H

#include "Random.h"

/**
 * Random number generator using a Exponential distribution.
 */
class ExponentialRandom: public Random {
public:
    ExponentialRandom(double beta);
    
    /** Creates and returns a new random double from an exponential distribution
     with beta as parameter*/
    double getNextRand();
    
    virtual ~ExponentialRandom();
private:

};

#endif	/* EXPONENTIALRANDOM_H */

