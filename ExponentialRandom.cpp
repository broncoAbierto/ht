
#include "ExponentialRandom.h"

ExponentialRandom::ExponentialRandom(double beta): Random(beta) {
    
}

double ExponentialRandom::getNextRand(){    
    double u;
    do{
        u = ((double)rand())/RAND_MAX;
        
    } while (u == 0);
    double x = -log(u)/beta;    
    
    return x;
}

ExponentialRandom::~ExponentialRandom() {
    
}

