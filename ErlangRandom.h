#ifndef ERLANGRANDOM_H
#define	ERLANGRANDOM_H

#include "Random.h"

/**
 * Random number generator using a Erlang distribution.
 */
class ErlangRandom: public Random {
public:
    ErlangRandom(double alpha, double beta);

    /** 
     * Creates and returns a new random double from an Erlang with alpha and 
     * beta as parameters.
     */
    double getNextRand();

    virtual ~ErlangRandom();
private:
    
    double alpha;
    
};

#endif	/* ERLANGRANDOM_H */

